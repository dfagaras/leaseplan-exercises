@available
Feature: Search for the available product

  Scenario Outline: Validate that at least one product is in the list
    When he calls the endpoint for the available product "<name>"
    Then he sees the there is at least one result in the products list
    Examples:
      | name   |
      | orange |
      | apple  |
      | pasta  |
      | cola   |

  Scenario Outline: Validate that each product url redirects to a secured resource
    When he calls the endpoint for the available product "<name>"
    Then he sees that all products pictures have a secure link
    Examples:
      | name   |
      | orange |
      | apple  |
      | pasta  |
      | cola   |