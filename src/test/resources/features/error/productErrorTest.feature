@unavailable
Feature: Validate unavailable product

  Scenario Outline: Validate unavailable product respond with expected error code
    When he calls the endpoint for the unavailable product "<productName>"
    Then he sees that error status code is "<statusCode>"
    Examples:
      | productName | statusCode |
      | pe ar       | 404        |
      | bottle      | 404        |
      | haha        | 404        |
      | 1           | 404        |

  Scenario Outline: Validate unavailable product respond with expected error message
    When he calls the endpoint for the unavailable product "<productName>"
    Then he sees that error message is "Not found"
    Examples:
      | productName |
      | pe ar       |
      | bottle      |
      | x           |
      | 1           |