package leaseplan.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class ErrorDetail {
    private boolean error;
    private String message;
    @JsonProperty("requested_item")
    private String requestedItem;
    @JsonProperty("served_by")
    private String servedBy;
}
