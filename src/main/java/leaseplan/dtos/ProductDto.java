package leaseplan.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
public class ProductDto {
    public String provider;
    public String title;
    public String url;
    public String brand;
    public String unit;
    public String promoDetails;
    public String image;
    public float price;
    public boolean isPromo;
}
