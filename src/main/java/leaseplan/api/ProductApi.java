package leaseplan.api;

import io.restassured.response.Response;
import leaseplan.dtos.ErrorDto;
import leaseplan.dtos.ErrorWrapper;
import leaseplan.dtos.ProductDto;
import leaseplan.dtos.ProductsDto;
import leaseplan.dtos.ResponseEntity;
import net.serenitybdd.rest.SerenityRest;

import java.util.Arrays;

public class ProductApi {

    public ProductsDto getProducts(String productType) {
        Response response = SerenityRest.given()
                .get(System.getProperty("local_url") + productType);
        return new ProductsDto(response.getStatusCode(), Arrays.asList(response.getBody().as(ProductDto[].class)));
    }

    public ErrorWrapper getProductsWithError(String productType) {
        Response response = SerenityRest.given()
                .get(System.getProperty("local_url") + productType);
        return new ErrorWrapper(response.getStatusCode(), response.getBody().as(ErrorDto.class));
    }

    //TODO: -create generic method for get product endpoint
    public <T> ResponseEntity<T> testResponseEntity(String productType, Class<T> expectedClass) {
        Response response = SerenityRest.given()
                .get("https://waarkoop-server.herokuapp.com/api/v1/search/demo/" + productType);
        return new ResponseEntity<>(response.getStatusCode(), Arrays.asList(response.getBody().as(expectedClass)));
    }
}
