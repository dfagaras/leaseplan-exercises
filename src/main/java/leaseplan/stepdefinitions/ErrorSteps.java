package leaseplan.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import leaseplan.api.ProductApi;
import leaseplan.dtos.ErrorDto;
import leaseplan.dtos.ErrorWrapper;
import net.thucydides.core.annotations.Steps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class ErrorSteps {

    @Steps
    private ProductApi productApi;
    private ErrorWrapper errorWrapper;

    @When("he calls the endpoint for the unavailable product {string}")
    public void heCallsEndpointForUnavailableProduct(String product) {
        errorWrapper = productApi.getProductsWithError(product);
    }

    @Then("he sees that error status code is {string}")
    public void heSeesThatErrorMessageStatusIs(String statusCode) {
        assertThat(errorWrapper.getStatusCode(), is(Integer.valueOf(statusCode)));
    }

    @Then("he sees that error message is {string}")
    public void heSeesTheNotFoundErrorMessage(String errorMessage) {
        assertThat(errorWrapper.getErrorDto().getDetail().getMessage(), is(errorMessage));
    }
}
