package leaseplan.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import leaseplan.api.ProductApi;
import leaseplan.dtos.ProductDto;
import leaseplan.dtos.ProductsDto;
import net.thucydides.core.annotations.Steps;
import org.hamcrest.Matchers;

import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.greaterThan;

public class ProductSteps {

    @Steps
    private ProductApi productApi;
    private ProductsDto productsDto;

    @When("he calls the endpoint for the available product {string}")
    public void heCallsEndpointForProduct(String product) {
        productsDto = productApi.getProducts(product);
    }

    @Then("he sees the there is at least one result in the products list")
    public void heSeesTheResultsDisplayed() {
        assertThat(productsDto.getProductDtoList().size(), greaterThan(0));
    }

    @Then("he sees that all products pictures have a secure link")
    public void heValidatesProductsPicturesLinks() {
        List<String> productPictureLinks = productsDto.getProductDtoList().stream().map(ProductDto::getUrl)
                .collect(Collectors.toList());
        assertThat(productPictureLinks, Matchers.everyItem(containsString("https://")));
    }
}
