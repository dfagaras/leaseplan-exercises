# Lease plan exercise automation project

* [General info](#general-info) 
* [Tools](#tools)
* [Project Setup](#project-setup)
* [Requirements](#requirements)
* [Building project](#building-project)
* [Running tests](#running-tests)
* [Test reports](#test-reports) 
* [What was refactored and added](#what-was-refactored-and-added) 




## General info
This is a java automation project covering backend APIs created with Serenity, Cucumber and Maven.
The scope of the project is to refactor it and make it run correctly on Gitlab CI

## Tools
* Maven
* Serenity
* Cucumber
* Lombok
* Hamcrest

## Project Setup

### 1.Setup your ssh key in GitLab

Navigate to [SSH Keys](https://gitlab.com/-/profile/keys) and hit `Add new key` button. If you generated one key before it can be found in ~/.ssh/id_rsa.pub.
For the purpose of this project you are not required to add the ssh since you are not added on the project yet ,  you can proceed to the next step and clone the project with `https`
#### Generate the SSH Key
Press start and open the `Command Prompt` , run ssh-keygen. The key will be generated in the folder mentioned above.
### 2.Clone project locally
Create a folder with the project name open the bash or navigate in the folder from command prompt and 
run `git clone <repo-url>`

```bash
git clone https://gitlab.com/dfagaras/leaseplan-exercises.git
```
Make sure that you are on the `main` branch
 
## Requirements

In order to run this project you need to have the following installed locally:
* Maven 3.8.6
* Java 11

## Building project
The maven command for building the project is :

      mvn clean verify -DskipTests=true

## Running tests

      mvn clean verify

## Test reports
The reports are generated after the tests are run , a link will be available in the run console but the report can also be found in the 
`target/site/index.html`

## What was refactored and added
* The pom was cleaned since there were dependency conflicts and some of them were not used
* The `distributionManagement` was deleted since not used
* Only `paralelClasses` was left since we cannot run classes and methods in parallel in the same time
* Threads was set to `2`
* All dependencies used were upgraded to the latest version
* Also, Hamcrest was added as a dependency for the assertion.
* The configuration in `maven-surfire-plugin` was deleted.
* The `systemPropertyVariables` in `maven-filesafe-plugin` was replaced by `systemPropertiesFile` and now in main/resources
  there is a property file from which variables can be read.
* Both test runners were added in the configuration
* A `check-feature-files` was added in `serenity-maven-plugin` to fail the build if error are found in the features files .


* Proper classes and packages were created 
* Readme.md was deleted from the main package and resources was added to main
* `stepdefinitions` package was moved to main.
* DTOs were created for error and api success response in order to manipulate the results.
* runners were created in the test package.
* Since not using gradle , everything related was deleted.
* The serenity.properties and sernity.conf were also deleted since not helping the purpose of the project.
* Two methods were created for `getProducts` and `getProductsWithError` that have on response the status code and the body.
* Steps were created for both error and success scenarios , the steps are dynamic.
* For not duplicating tests , `scenario outline` was used , so we have 16 scenarios verified in 4 tests.
* The tests will run automatically every morning at 9:50 and when a merge is made.


* For the testing part i've decided to check all the options provided as available by making sure that at least one result
is returned for each product.
* Other positive test was to check that every product link is secured.
* For the negative test i've checked that the error message and error code are the expected one.
* If tests ar run there will be a failure since `apple` is an available product but returns nothing.

